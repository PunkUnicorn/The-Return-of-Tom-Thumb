# Chapter Six

“Here it is.”, The Tailor pointed towards the town ahead. Tom wanted to stop and rest but The Tailor sped up. “I need to see the smithy, get a few things while we're here.”, but he did not mention the stench of rotting vegetables.

Somebody slumped on the floor against the side a building, blocking their path. “He's drunk.”: The Tailor explained. They maneuvered single file around him.

Weaving through accidental alleyways, squeezing past competing buildings, they startled rats which darted away in panic. Judging by the size of the rats, Tom knew people here ate well. That combined with the smell of waste.

Leaving the maze between buildings, they entered a muddy clearing which soon led to a high, intimidating wooden wall. The Tailor seemed to know where he was going but walked straight towards the wall. But then Tom could see a door blended into it. She could hear a crowd on the other side and as The Tailor opned the door the sounds and sights of the crowd hit them.

Bustling people shuffled around market stalls. The stalls themselves stretched into the distance. What looked like an oppressive wall on one side stopped short, in ruins, before it could even make a semicircle around the market.

The Tailor stepped to one side and raised an arm pausing Tom. He looked across the stalls. She could smell chicken being cooked, and a further away, over the sound of the crowd, a piper played. Somebody near them shouted in their direction, Tom quickly looked up. But it had nothing to do with them, it was for somebody else. The Tailor took a deep breath: “This is where trappers and miners meet.”, he shared, regretfully. “Come on, let's get this over with.”

Tom took stride behind The Tailor, it was easier following his steps through the crowd. Her attention lost to the busyness around her, each stall a world of it's own. One had arrays of hanging, dried meats. Its neighbour: a stall displaying increasing sizes of metal hoops. Long, thin, wooden poles lay next to them. Tom wondered what these were for?

The other side of the muddy walkway, through a happenstance gap in the crowd, a vendor surrounded by tall bolts of multicoloured cloth was arguing with a rotund customer. Someone behind them laughed but the cloth merchant took her attension again, raising his shrill voice; it pierced through the crowd in a language Tom did not understand.

Hurring past all of the distractions they walked towards a steady, metallic banging. It had just started; pulses of three. Each hit rattled the back of Toms teeth, but its echo played a beautiful tone. “Oh good,”, The Tailor exclaimed, “he's here.”. 

The Tailor extended his arm towards Tom, “Climb up.”, and Tom scrambled up The Tailor's side to his shoulders, easily finding steps along his side. The view was spectacular, an image she would never forget: the variety of people, clothes, and fully ladened trestle bench stalls. The crowed bobbed, a sea of heads. A group of fine hats with shimmering feathers standing out from the crowd.

From The Tailors shoulders she traced the sound of the banging: a large circular brick building in the center of the market, thick smoke plumed from it's chimneys.
