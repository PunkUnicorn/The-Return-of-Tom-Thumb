# Chapter Two

Tom Thumb had been back home for several nights. As usual her thoughts were occupied, replaying the past troubling days: the terrifying giant, coming to terms with their parents leaving them to die in the forest.

The worst, however, was being singled out as a girl in front of her brothers. She (and her brothers) had not worried about this before, despite her father’s intimidating insistence he only had sons. But the words of the motherly giant echoed in her thoughts: “You think you can hide it? Oh, beautiful young thing, you can't hide it.”

Tom felt the surprise and shock again. For all the times she believed she had come to terms with this, it would return to unsettle her.

Craving a distraction, she glanced up to see what remained of the giant apple: it was thinner now, and mushy. She and her brothers had dragged this apple from the giant’s garden a few days ago. Safe at home Tom reflected on these strange events, back to the time they decided to take the apple. “My idea.” she remembered.

More than ever, she was aware her actions had saved them from starvation. The sense of relief was mixed with the anxiety of how close they were to leaving it behind. And yet amongst the fragile relief was a small, glimmering sense of pride.

Wiggling her toes, she felt the exquisite comfort in the soft leather of her boots. She could do that all day. She had been doing that all day.

But now she had to focus on the present, which drew her attention back to the apple. It would keep them fed for two more days. The heavenly comfort of having enough food came with a nagging feeling. What was that nagging feeling? “Oh,” she thought, “it's only going to last two more days.”

With a sigh she pulled herself up to sit. The grass stalk in her mouth, now an inconvenience, spat out. “What I need is some kind of plan.”, she thought.

Soon she had an idea: one thing was clear, another giant apple would solve the hunger problem. It was something. And then a moment of inspiration: a hidden treasure she had missed: the giant apple pips.

Jumping to her feet, she hurried towards the core. It was almost as tall as her. She paced around, looking for a good place to start. As quickly as she found a likely spot, she squared her knuckles and punched a level blow, ripping through to the seed chamber. Gripping the breach, she tore it wider.

Sticking her arm in, the pulp ran up her sleave. She felt a pip. Pushing further, as far as possible, she mapped the pip's orientation. With a concerted shunt, the pip shifted to a better grip and she eased it out. Feeling triumphant, she freed another two and fitted all three of the large pips into her pockets.
