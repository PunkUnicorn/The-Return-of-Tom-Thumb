# Chapter Five

Walking along a rough path, a field on one side, a short stone wall the other, The Prince could barely keep his eyes open. He had been walking for a short time.

The wall stopped at a grove. On one side the field continued, the other: a thick meadow seemed to go on forever. A fallen tree invited him to sit down, promising a comfortable seat. He had not seen anyone else since leaving the old man's hut, and he was too fatigued to care about being recognised. He took his helmet off, resting it against one of the fallen tree's branches.

Working his pack off his back, he sat on the fallen tree and almost instantly fell asleep.

When The Prince woke the wind had picked up, the branches whistled. Clouds had gathered, it looked like a storm might be coming. This was exciting, a chance to use his equipment. But more than that, he felt that his life on the road was beginning.

With his gauntlets off, he pried the pack's buckles open. The canvas backpack was soaked through, the straps were difficult to work free.

Unbuckled, he pulled out his large, tarred canvas sheet. It would make his tent. Unwieldy in the wind, the large sheet flapped wildly. The first peg driven quickly into the ground with a stomp. He fastened the other corner of the sheet to the edge of his makeshift seat. It was a crude bivouac, but it should suffice. Some more driven pegs and The Prince thought he had built enough of a shelter to withstand the weather.

Inside the makeshift shelter, shielded from the wind, he nestled down and listened to his internal chatter. It fed him concerns which he dismissed; the isolation giving him safety. Wind outside barraged unsuccessfully, whipping aggressively. But the pegs, the rope and the canvas won decisively. With each victory The Prince fell deeper and deeper into sleep.

Birdsong woke him. He peered out of the tent; it was afternoon, the sky had cleared. He felt more awake.

The winter sunshine was on his face and he felt good. There was no storm earlier bringing relief of easier travelling, hampered only by a slight disappointment; it was not a true test.

The tarred sheet did not pack well. Methodically rolling it tight, there was a figure in the distance. He could not see much about them, except for their striking red robe. It was a good sign though, seeing someone travelling from the direction he was going. The trail did, at least, go somewhere.

By the time he had finished rolling the sheet, the figure was close enough to see, but too far away to speak with. It was a young woman, she lowered her red hood and waved cheerfully. He interrupted his two handed packing to respond with a quick knight's salute.

Once the taught, canvas bundle was crammed back into his backpack, and the straps pulled tight, he squatted to shoulder the straps, taking a breath before bearing the full weight.

“Hello.” the young woman interrupted.

“Greetings.”, he replied while shuffling the weight of his pack, adjusting the straps. Then, as fast as possible he grabbed his helmet off the fallen tree and put it on, covering his head. He left the visor open.

“Are you making a camp?” she asked.

“No, I've just packed everything up.”

She seemed disappointed. “Are you travelling north?” she asked hopefully.

“Yes.”, he offered, but she had been travelling in the opposite direction.

Before he could think what to say she quickly asked: “Would you join me? We could travel together.”
  
“I'm not sure if you're aware, you were travelling southwards. I'm going the way you came.”; he felt he had to say that.

“I've been lost for a while. I wanted to avoid the main roads, but it's hard to keep track of direction, ...I'm not on trails I'm familiar with.”

The Prince nodded in understanding, although he himself knew several ways to identify north. Not everyone had such training. “To accompany you would be a duty and an honour.” he responded earnestly.

She looked The Knight up and down, balancing her weight on one leg, the other crossed behind it on point. She held her hands behind her back. “That's what I thought.”, the conclusion carrying her satisfaction. “Are you a knight errant?”

“Yes.”, his disguise working, “I am.”

She seemed impressed. “I'm on an adventure myself, I need to get far north, and I can't use the main routes. I do what I want now.”

The Knight nodded along with her words, as if they made sense. He was exhilarated to have some travelling company; better cover for his escape.

“I'm Red,” she beamed a warm, welcoming smile, “I'm really pleased you're joining me.”

But not as pleased as The Knight, it was all going rather well. Their common goal: to travel far north and stay off the main trails.

As they walked together, along the way Red had come from, The Knight provided conversation: replaying his rehearsed lines, where he had come from, his knight's vows and how these may effect his behaviour. Red offered no doubt, she was delighted with all parts of his script.

Once he had finished his well rehearsed spiel, a comfortable silence settled. Red pointed ahead; “There's a beautiful spot on the edge of the meadow, a small stream next to a clearing. It's hidden by a bramble thicket. I'm surprised I found it.”

To The Knight this sounded ideal.

- - -

Breakfast was as delicious, if not more so, than the stew the previous evening: eggs, bacon, dark bread and butter. Tom sneaked what she could into her pocket; a sausage. There was no way to put the mashed potato in her pocket, so she finished the lot. Potato had never tasted so good. “Do you eat like this all the time?”.

“I try not to.”, he replied, pushing his empty plate forward. He cocked his chair on two legs, leaning his back against the wall, instead of the chair's back.

“You asked me where I was going?”, he shuffled into a more relaxing position. “I'm a travelling tailor, I just travel.”

Then, as if he had remembered something, he added: “And I do tailoring.”, using his finger to accentuate.

“What's tailoring?”; she felt she could ask him.

Leaning forward, mirthfully: “I thought you would never ask. Let's go and show you tailoring.”

And he got up and walked towards the inn's door, beckoning Tom with his head; “Come on.”. A jaunty walk. He was enjoying this.

Outside, Tom stood ridiculously. Her arms out straight and her legs apart. The Tailor worked under her arm with pins in his mouth. “And I'll do something with your hair.” he tried to say.

“What are you doing?”, the tone of desperation giving The Tailor a clue; she probably wanted more explanation than the word 'tailoring'. He took the pins out of his mouth.

“I'm bringing your jumper in, so it fits better round the shoulders, and waist.”, then he paused, pursing his lips, finally adding: “I say fits better, but what I mean is: fits better than not fitting whatsoever.”

She felt exposed standing like this, but she trusted what The Tailor was doing; it must be important for travelling. He continued to sew around each underarm, and similarly on her trousers.

He drew her away, from the small pile of fabric she was now standing in, and sat her down,  explaining: “Now to sort your hair out!”.

She managed to look at what he had done to her jumper. It was less baggy, with a thick long stitch. It was well balanced against the other under-arm alteration. The excess length of trouser leg had been cut off and seamed, instead of roll-ups. But there was no opportunity to look closer; between looks, her head position was adjusted by a gentle hand. Sometimes, but not always, the touch was followed by the sound of snipping.

“There...”, and he brought a small mirror to her face. Her hair had defined steps, but was still shaggy. “Just a tidy.”, he underplayed, and then paused. For a moment, he almost looked like he was going to say something, but it passed.

Moving her arms around, testing, she was surprised at the lighter feeling. “That's good.”, she said, instantly regretting the vocal impulse. Kicking her legs, she inspected each landing. Now her trousers covered her boots more. That was convenient.

The Tailor looked at her expectantly, as if she was going to elaborate. But she nodded with the minimum of approval. “The next town's not far”, he said, changing the subject, “we'll make it before lunch.”, and he turned southwards: “It's got the third worst inn in the world., You have to see it to believe it. Lets go.”

They started walking. Suddenly, Tom had no idea what to expect.

- - -

Red and The Knight sat by the stream. The fire was building strength; wet blankets, from The Knights pack, propped up with sticks to capture the warmth. The half empty pack itself, planted just beyond the fire's range, sat as a center-piece to the evenly strewn contents; a tiny city of equipment. Two small tents, in parallel, looked ready; their entrance sheets open in identical fashion.

The midday sun felt good, and with the fire's warmth, The Knight started to feel like he could be warm again. Red had pulled her hood over her head and was humming a gentle melody. Listening, he heard contentment. It elevated the mood. The stream was interesting viewing. Too shallow for fishing, he will be cooking his standard issue rations tonight.

Red finished humming, the crackle of the fire taking main stage; popping a spark, it gently collapsed on itself. The Knight pushed himself off the ground and added three logs. A quick appraisal and he turned back to the stream, resuming his place next to Red.

There was no talking. The Knight took mental inventory, thinking through his groups of equipment: cooking, cleaning, engineering, repairing, fighting. Having set up camp, he was more aware that his items were now his home and his life.

Red hunched over a small, open papyrus codex. The feather end of her raven quill exaggerating the slow, careful swirling of the writing. The Knight noticed a tiny, elegant ink bottle tied to a cord around her neck. He himself had some ink, sealing wax and parchment, but it was for official knight's business only. Like his, her feather was a pinion; an optimum feather for a quill, he was somewhat impressed.

As the afternoon drew in, the blankets dried and the fire became a stove. They dined on a knight's banquet of dried meats, pickled vegetables and fruit preserves. Despite his earlier rest he began to feel fatigue catch him up. “I'll take an early rest. I'll be awake again in the early hours to take over the watch.”. Red understood and was fine with this arrangement. She placed two more small logs on the fire and slouched back, watching the fresh logs adapt to their new home.
